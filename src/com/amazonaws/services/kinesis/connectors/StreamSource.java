package com.amazonaws.services.kinesis.connectors;

/*
 * Copyright 2013-2014 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Amazon Software License (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 * http://aws.amazon.com/asl/
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ejml.simple.SimpleMatrix;

import com.amazonaws.services.kinesis.connectors.KinesisUtils;
import com.amazonaws.services.kinesis.connectors.KinesisMessageModel;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.regions.RegionUtils;
import com.amazonaws.services.kinesis.AmazonKinesisClient;
import com.amazonaws.services.kinesis.connectors.KinesisConnectorConfiguration;
import com.amazonaws.services.kinesis.model.PutRecordRequest;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.neural.rnn.RNNCoreAnnotations;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.sentiment.SentimentCoreAnnotations;
//import edu.stanford.nlp.sentiment.SentimentCoreAnnotations.SentimentAnnotatedTree;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.util.CoreMap;

import twitter4j.FilterQuery;
import twitter4j.GeoLocation;
import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.User;
import twitter4j.conf.ConfigurationBuilder;

/**
 * This class is a data source for supplying input to the Amazon Kinesis stream. It reads lines from the
 * input file specified in the constructor and emits them by calling String.getBytes() into the
 * stream defined in the KinesisConnectorConfiguration.
 */
public class StreamSource implements Runnable {
    private static Log LOG = LogFactory.getLog(StreamSource.class);
    protected AmazonKinesisClient kinesisClient;
    protected KinesisConnectorConfiguration config;
    protected final String inputFile;
    protected final boolean loopOverInputFile;
    protected ObjectMapper objectMapper;

    /**
     * Creates a new StreamSource.
     * 
     * @param config
     *        Configuration to determine which stream to put records to and get {@link AWSCredentialsProvider}
     * @param inputFile
     *        File containing record data to emit on each line
     */
    public StreamSource(KinesisConnectorConfiguration config, String inputFile) {
        this(config, inputFile, false);
    }

    /**
     * Creates a new StreamSource.
     * 
     * @param config
     *        Configuration to determine which stream to put records to and get {@link AWSCredentialsProvider}
     * @param inputFile
     *        File containing record data to emit on each line
     * @param loopOverStreamSource
     *        Loop over the stream source to continually put records
     */
    public StreamSource(KinesisConnectorConfiguration config, String inputFile, boolean loopOverStreamSource) {
        this.config = config;
        this.inputFile = inputFile;
        this.loopOverInputFile = loopOverStreamSource;
        this.objectMapper = new ObjectMapper();
        kinesisClient = new AmazonKinesisClient(config.AWS_CREDENTIALS_PROVIDER);
        kinesisClient.setRegion(RegionUtils.getRegion(config.REGION_NAME));
        if (config.KINESIS_ENDPOINT != null) {
            kinesisClient.setEndpoint(config.KINESIS_ENDPOINT);
        }
        KinesisUtils.createInputStream(config);
    }

    @Override
    public void run() {
        int iteration = 0;
        do {
            InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("users.txt");
            if (inputStream == null) {
                throw new IllegalStateException("Could not find input file: " + inputFile);
            }
            if (loopOverInputFile) {
                LOG.info("Starting iteration " + iteration + " over input file.");
            }
            try {
                processInputStream(inputStream, iteration);
            } catch (IOException e) {
                LOG.error("Encountered exception while putting data in source stream.", e);
                break;
            }
            iteration++;
        } while (loopOverInputFile);
    }

    /**
     * Process the input file and send PutRecordRequests to Amazon Kinesis.
     * 
     * This function serves to Isolate StreamSource logic so subclasses
     * can process input files differently.
     * 
     * @param inputStream
     *        the input stream to process
     * @param iteration
     *        the iteration if looping over file
     * @throws IOException
     *         throw exception if error processing inputStream.
     */
    protected void processInputStream(InputStream inputStream, int iteration) throws IOException {
        // while ((line = br.readLine()) != null) {
        int lines = 0;
    	ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setDebugEnabled(true);
	    cb.setOAuthConsumerKey(Constants.OAUTH_CONSUMER_KEY);
	    cb.setOAuthConsumerSecret(Constants.OAUTH_CONSUMER_SECRET);
	    cb.setOAuthAccessToken(Constants.OAUTH_ACCESS_TOKEN);
	    cb.setOAuthAccessTokenSecret(Constants.OAUTH_ACCESS_TOKEN_SECRET);
        while (lines < 5) {

		    TwitterStream twitterStream = new    TwitterStreamFactory(cb.build()).getInstance();

		    StatusListener listener = new StatusListener() {

		        @Override
		        public void onException(Exception arg0) {
		            // TODO Auto-generated method stub

		        }

		        @Override
		        public void onDeletionNotice(StatusDeletionNotice arg0) {
		            // TODO Auto-generated method stub

		        }

		        @Override
		            public void onScrubGeo(long arg0, long arg1) {
		            // TODO Auto-generated method stub

		        }

		        @Override
		        public void onStatus(Status status) {
		            User user = status.getUser();

		            System.out.println("=============Start==================");
		            /******************
		            ** Gets Username **
		            ******************/ 
		            String username = status.getUser().getScreenName();
		            System.out.println("Username: "+username);
		            
		            /******************
		            ** Gets Location **
		            ******************/
		            String profileLocation = user.getLocation();
		            profileLocation = profileLocation.replaceAll("[\n\r]", "");
		            profileLocation = profileLocation.replaceAll("[\\|]", "");
		            profileLocation = profileLocation.replaceAll("[\"]", "");
		            profileLocation = profileLocation.replaceAll("'", "");
		            System.out.println("Profile Location: "+profileLocation);
		            
		            /******************
		            ** Gets Tweet ID **
		            ******************/
		            long tweetId = status.getId(); 
		            System.out.println("Tweet ID: "+tweetId);
		            
		            /***************
		            ** Gets Tweet **
		            ***************/
		            String content = status.getText();
		            content = content.replaceAll("[\n\r]", "");
		            content = content.replaceAll("[\\|]", "");
		            content = content.replaceAll("[\"]", "");
		            content = content.replaceAll("'", "");
		            System.out.println("Content: "+content);
		            content = Normalizer.normalize(content, Normalizer.Form.NFD);
		            content = content.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
		            
		            /***********************
		            ** Gets Creation Date **
		            ***********************/
		            Date dateCreated = status.getCreatedAt();
		            //System.out.println(new SimpleDateFormat("yyyy-mm-dd HH:mm:ss").format(dateCreated));
		            String newDateForm = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(dateCreated);
		            String date = newDateForm+""; 
		            System.out.println("Date Created: "+date);
		            
		            /************************
		            ** Gets Favorite Count **
		            ************************/
		            int favCount = status.getFavoriteCount();
		            System.out.println("Favorites Count: "+favCount);
		            
		            /************************
		            ** Gets Retweet Flag   **
		            ************************/
		            boolean retweet = status.isRetweet();
		            System.out.println("Retweet? : "+retweet);
		            
		            /************************
		            ** Gets Retweet Count  **
		            ************************/
		            int retweetCount = status.getRetweetCount();
		            System.out.println("Retweet Count: "+retweetCount);
		            
		            /******************
		            ** Gets Language **
		            ******************/
		            String language = status.getLang();
		            System.out.println("Language: "+language);
		            
		            /*********************
		            ** Gets GeoLocation **
		            *********************/
		            GeoLocation geolocation = status.getGeoLocation();
		            System.out.println("Geolocation: "+geolocation +"\n");
		           
		            
		            System.out.println("===============End================");
		            
		            if(language.equals("en")){
		            	
			            System.out.println("===============Sentiment-Stanford NLP================");
		            	float sentimentScore = extractSentiment(content);
		            	System.out.println("Sentiment Score "+sentimentScore);
			            System.out.println("===============Sentiment-Stanford NLP================");
			            
			            System.out.println("===============Sentiment-SentiWordNet================");
		            	float sentimentScoreSWN =sentimentCalculator(content);
		        		System.out.println(sentimentScoreSWN);
		        		String satisfactionLevel = getSatLevel(Double.toString(sentimentScoreSWN), Double.toString(sentimentScore));
		        		System.out.println(satisfactionLevel);
			            System.out.println("===============Sentiment-SentiWordNet================");
			            
		            	String line = "{\"date_created\":\""+date+"\"" 
		            			+",\"tweetid\":\""+tweetId+"\"" 
		            			+",\"tweet\":\""+content+"\""
		            			+",\"username\":\""+username+"\""
		            			+",\"profile_location\":\""+profileLocation+"\""
		            			+",\"favorite_count\":"+favCount
		            			+",\"retweet\":"+retweet
		            			+",\"retweet_count\":"+retweetCount
		            			+",\"language\":\""+language+"\""
		            			+",\"geolocation\":\""+geolocation+"\""
		            			+",\"sentiment_stn\":\""+sentimentScore+"\""
		            			+",\"sentiment_swn\":\""+sentimentScoreSWN+"\""
		            			+",\"sat_level\":\""+satisfactionLevel+"\""
		            			+"}";
		            	System.out.println(line);
		            	KinesisMessageModel kinesisMessageModel = null;
						try {
							kinesisMessageModel = objectMapper.readValue(line, KinesisMessageModel.class);
			                PutRecordRequest putRecordRequest = new PutRecordRequest();
			                putRecordRequest.setStreamName(config.KINESIS_INPUT_STREAM);
			                putRecordRequest.setData(ByteBuffer.wrap(line.getBytes()));
			                putRecordRequest.setPartitionKey((kinesisMessageModel.getTweetID()));
			                kinesisClient.putRecord(putRecordRequest);
						} catch (com.fasterxml.jackson.databind.JsonMappingException e){
							System.out.println("Out of UTF-8, excluded tweet");
						} catch (IOException e) {
							System.out.println("Out of UTF-8, excluded tweet");
						}
		            
		            }
		            
		        }

		        @Override
		        public void onTrackLimitationNotice(int arg0) {
		            // TODO Auto-generated method stub
		            System.out.println("onTrackLimitationNotice" +"\n");

		        }

		        @Override
		        public void onStallWarning(StallWarning arg0) {
		            // TODO Auto-generated method stub
		            System.out.println("onStallWarning" +"\n");

		        }
		        
		        
		        
		     // Function to extract sentiments
		    	public float extractSentiment(String tweet){
		    		float overallSentiment = 0;
		    		int intervals = 0;
		    		overallSentiment += removeSpecialCharsAndGetSentiment(tweet);
		    		if(overallSentiment==0){
		    			return 0;
		    		}
		    		return overallSentiment;
		    	}
		    	
		    	// Function to remove special characters in the string
		    	private float removeSpecialCharsAndGetSentiment(String line){
		    		String[] lines;
		    		lines = line.split("\\|");
		    		float sentiment = 0;
		    		int sum=0;
		    		for(int i=0;i<lines.length;i++){
		    			String tempLine = lines[i].trim();
		    			if(!tempLine.endsWith(Constants.FULL_STOP))
		    				tempLine = tempLine + Constants.FULL_STOP;
		    			if(tempLine.length()>0 && isValidLine(tempLine)){
		    				sentiment+= getSentimentForLine(tempLine);
		    				sum++;
		    			}
		    		}
		    		if(sum==0 || sentiment==0)
		    			return 0;
		    		return sentiment/sum;
		    	}
		    	
		    	// Function to check if it is a valid line
		    	private boolean isValidLine(String tempLine) {
		    		if(stringSpecialChars(tempLine).length()>0)
		    			return true;
		    		else
		    			return false;
		    	}
		    	
		    	// It is a core function that gets the sentiment for given line
		    	private float getSentimentForLine(String string) {
		    		Properties props = new Properties();
		    		props.setProperty("annotators", "tokenize,ssplit, parse, sentiment");
		    		props.setProperty("sentiment.model", "edu/stanford/nlp/models/sentiment/sentiment.binary.ser.gz");
		    		StanfordCoreNLP pipeline = new StanfordCoreNLP(props);
		    		float mainSentiment = 0;
		    		if (string != null && string.length() > 0) {
		    			Annotation annotation = pipeline.process(string);
		    			for (CoreMap sentence : annotation.get(CoreAnnotations.SentencesAnnotation.class)) {
		    				Tree tree = sentence.get(SentimentCoreAnnotations.AnnotatedTree.class);
		    				SimpleMatrix sentiment = RNNCoreAnnotations.getPredictions(tree);
		    				mainSentiment = (float)sentiment.get(1);
		    			}
		    		}
		    		return mainSentiment;
		    	}
		    	
		    	public String stringSpecialChars(String orig){
		    		StringBuilder sb = new StringBuilder();
		    	    for (int i = 0; i < orig.length(); i++) {
		    	      char c = orig.charAt(i);
		    	      if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c >= '0' && c <= '9') || c==' ') {
		    	        sb.append(c);
		    	      }
		    	    }
		    	    return sb.toString();
		    	}
		    	
		    	public String getSatLevel(String sentimentScore, String sentimentScoreNLP){
		    		float averageScore = Float.parseFloat(sentimentScore);
		    		float averageScoreNLP = Float.parseFloat(sentimentScoreNLP);
		    		if((averageScore < 0 && averageScore>=-0.25) || averageScoreNLP < 0.3)
		    		    return "negative";
		    		else if((averageScore < -0.25 && averageScore>=-0.5) || averageScoreNLP < 0.3)
		    		    return "negative";
		    		else if((averageScore<-0.5) || averageScoreNLP < 0.2)
		    		    return "very negative";
		    		else if(averageScore>=0.75)
		    		    return "very positive";
		    		else if(averageScore > 0.25 && averageScore<0.5)
		    		    return  "positive";
		    		else if(averageScore>=0.5)
		    		    return  "positive ";
		    		return "neutral";
		    	}
		    	
		    	public double extract(String word)
		        {
		    		Double total = new Double(0);
		    	    if(RedshiftBasicExecutor.dictionary.get(word+"#n") != null)
		    	         total = RedshiftBasicExecutor.dictionary.get(word+"#n") + total;
		    	    if(RedshiftBasicExecutor.dictionary.get(word+"#a") != null)
		    	        total = RedshiftBasicExecutor.dictionary.get(word+"#a") + total;
		    	    if(RedshiftBasicExecutor.dictionary.get(word+"#r") != null)
		    	        total = RedshiftBasicExecutor.dictionary.get(word+"#r") + total;
		    	    if(RedshiftBasicExecutor.dictionary.get(word+"#v") != null)
		    	        total = RedshiftBasicExecutor.dictionary.get(word+"#v") + total;
		    	    return total;
		        }
		    	
		    	// For SentiWordNet
		    	public float sentimentCalculator(String chat){
		    		String[] words = chat.split("\\s+");
		    		double totalScore = 0;
		    		float averageScore;
		    		for(String word : words) {
		    		    word = word.replaceAll("([^a-zA-Z\\s])", "");
		    		    if (extract(word) == 0)
		    		        continue;
		    		    totalScore += extract(word);
		    		}
		    		averageScore = (float) totalScore;
		    		return averageScore;
		    	}

		    };
   
	        /*******************************
			 *Tweets based on keyword ASU *
			 *******************************/
		    String keywords[] = {" ASU "};
		    FilterQuery fq = new FilterQuery();
	        fq.track(keywords);
	        twitterStream.addListener(listener);
	        twitterStream.filter(fq);
	        
	        /*************************************
			 *Tweets based on latitude longitude *
			 *************************************/
		    double lat = 53.186288;
		    double longitude = -8.043709;
		    double lat1 = lat - 0.1;
		    double longitude1 = longitude - 8;
		    double lat2 = lat + 0.1;
		    double longitude2 = longitude + 8;
		    double[][] bb = {{longitude1, lat1}, {longitude2, lat2}};
	        //fq.locations(bb);
	        //twitterStream.filter(fq);  
	        
			/*******************************
			 *Tweets based on Hashtag #ASU *
			 *******************************/
	        /*Twitter twitter = new TwitterFactory().getInstance();
	        try {
	            QueryResult result = twitter.search(new Query("#ASU"));
	            System.out.println(result);
	        } catch (TwitterException te) {
	            te.printStackTrace();
	        }*/
		    lines++;
        }

        }
   // }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
    }
}
