# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* For streaming tweets using Twitter4J, sending the streams through Kinesis to S3 and Redshift.

### How do I get set up? ###

* Export the project as a runnable JAR with dependencies option
* Run the jar using the following command giving in the path to text file containing the words for sentiment analysis:
nohup java -jar ASUTweetAnalyzer.jar /home/mohan/Twitter/SentiWordNet_Prod.txt & > log.txt